/**
 * Created by Rainc on 14-12-9.
 */
var express = require('express');
var app = module.exports = express();
var config = require('./etc/config.json');
var fs = require('fs');
var exec = require('child_process').exec;
var appServer = require(config.appServerPath);
var callBillServer = require(config.callBillServerPath);
var stateCtrl = require('./controllers/stateCtrl.js');
var heartbeatServer = require(config.heartbeatServerPath);
var router = require('./routers');
var log4js = require('log4js');
var bodyParser = require('body-parser');

log4js.configure({
  appenders: [
    { type: 'console' }, //控制台输出
    {
      type: 'dateFile', //日志文件输出
      filename: config.webLogFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'WEBSERVER'
    }
  ],
  replaceConsole: true
});
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

//app.configure
//app.use(log4js.connectLogger(logger, {level:'auto', format:':remote-addr :method :url'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public');
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

//路由
router(app);

var pidFilePath = config.pidFilePath;

//fs.exists(pidFilePath, function (exists) {
//  if(!exists){
    fs.open(pidFilePath,'a+',function(err){
      if(err){
        logger.error(err);
      }else{
        fs.readFile(pidFilePath, 'utf8', function (err, data) {
          if (err) {
            logger.error(err);
            return;
          }else if(!data){
           runApp();
          }else{
//            console.log(data);
            var cmd = "ps -ax | grep "+ data + " | awk '{ print $1 }'";
            var isPidActive = false;
            exec(cmd,
              function (error, stdout, stderr) {
                if (error !== null) {
                  logger.error('exec error: ' + error);
                }else if(stdout){
//                  console.log(stdout);
                  var result = stdout.split('\n');
                  for(var i in result){
                    if(data == result[i]){
                      isPidActive = true;
                      break;
                    }
                  }
//                  console.log(JSON.stringify(result));
                  if(isPidActive == false){
                    runApp();
                  }else{
                    logger.info("disableCall server is running!Please stop it before to run another one!");
                    process.exit();
                  }
                }else{
                  runApp();
                }
              });
          }
        })
      }
    })
//  }
//});

function runApp(){
  appServer.init(function(err,obj){
    if(!err){
      product_id = obj.product_id;
      server_id = obj.server_id;
      server_version = obj.server_version;
      timing_del_call_bill = config.timing_del_call_bill;    //设置话单保留时间
      account = config.account;                 //读取默认用户名
      password = config.password;               //读取默认密码

      app.listen(config.web_port);     //启动web server

      //将当前pid写入pid文件中
      var pid = process.pid;
      fs.writeFile(pidFilePath, pid, function (err) {
        if (err){
          logger.error('启动写入pid文件出错！');
        }else{
          logger.info('Pid it\'s saved!'); //文件被保存
        }
      });

      //启动心跳server
      heartbeatServer.init(function(err){
        if(err){
          logger.error('心跳服务启动失败！');
        }
      })

      //将状态信息写入数据库
      stateCtrl.insertIntoTable();

      //开启话单入库服务
      callBillServer.init(function(err){
        if(err){
          logger.error('话单入库服务启动失败！');
        }
      });

      logger.info("Web server listening on port %d in %s mode", config.web_port, app.settings.env);
      logger.info("You can debug your app with http://" + config.web_host + ':' + config.web_port);
    }else{
      logger.error(err);
    }
  });
}



