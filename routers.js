/**
 * Created by Rainc on 15-1-4.
 */

var api = require('./controllers/api');

module.exports = function (app) {
  app.get('/',function(req,res){
    res.render('index.html')
  });

  app.get('/api',api.disableCallApi);
  app.post('/api',api.disableCallApi);
}