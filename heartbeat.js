/**
 * Created by Rainc on 2015/5/28.
 */
var dgram = require('dgram');
var config = require('./etc/config.json');
var radius = require('radius');

var udp4 = dgram.createSocket('udp4');
exports.init = function(cb) {
  udp4.bind(config.heartbeat_port, config.server_ip);

  udp4.on("error", function (err) {
    console.log("heartbeat udp server error:\n" + err.stack);
    udp4.close();
    cb(err);
  });

  udp4.on('listening', function () {
    var address = udp4.address();
    console.log('Heartbeat Server listening on ' + address.address + ":" + address.port);

    cb(null);
  })
};

udp4.on('message', function (message, remote) {
  do_action(message,remote);
});

function do_action(message,remote){
  var packet = radius.decode_without_secret({packet: message,secret:'',no_secret:true});

  if (packet.code != 'Status-Server') {
    console.log('Heartbeat Server unknown packet type: ', packet.code);
    return;
  }

  //回复确认信息
  var response = radius.encode_response({
    packet: packet,
    code: 'Status-Client',
    secret:'frequency and call bill'
  });
  udp4.send(response, 0, response.length, remote.port, remote.address, function (err, bytes) {
    if (err) {
      console.log('Error Heartbeat sending response to ', remote);
    }else{
      //console.log('send ' +  remote.address)
    }
  });
}