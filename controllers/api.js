/**
 * Created by Rainc on 15-1-6.
 */

var fs = require('fs');
var loadCache = require('../appServer');
var loadFrequency = require('../callBillServer');
var sendTo= require('../sendToRemote');
var async = require('async');
var mysql1 = require('./basedao.js');

var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

exports.disableCallApi = function(req,res){
  var action = req.query.action;
  if(!action || action == ''){
    res.json({success:false,code:"require param action!"})
  }else{
    try{
      switch(action){
        case 'updateTblNum':
          var uuid = req.query.uuid;
          loadCache.loadCache(uuid,function(err){
            if(err != null){
              logger.error(err);
              res.json({success:false,code:err});
            }else{
              res.json({success:true,code:0});
            }
          });
          break;
        case 'updateTblFrequency':
          var uuids = req.query.uuids;
          var domain_uuid = req.query.domain_uuid;
          loadFrequency.loadFrequency(uuids,domain_uuid,function(err){
            if(err != null){
              logger.error(err);
              res.json({success:false,code:err});
            }else{
              res.json({success:true,code:0});
            }
          });
          break;
        case 'getStatus':
          //read from file
//          fs.open(status_path,'r',function(err){
//            if(err){
//              logger.error(err);
//            }else{
//              fs.readFile(status_path, 'utf8', function (err, data) {
//                if (err) {
//                  logger.error(err);
//                }else if(data){
          var data = GLOBAL.g_app_status;
          var num = 0;
          for(var i in data){
            num++;
          }
          if(num != 0){
            res.json({success:true,code:data});
          }
//                }
//              });
//            }
//          })
          break;
        case 'sendToCloud':
            sendTo.dcSendTo(function(err){
              //处理完成后修改服务器状态
              var sql = "update tbl_sys set dm_run_status="
              if(err){
                res.json({success:false,code:err})
                sql += "6 where uuid=" + server_id;
              }else{
                res.json({success:true,code:0})
                sql += "3 where uuid=" + server_id;
              }
              mysql1.Cb_process(sql,null,function(err){
                if(err){
                  logger.error('更新服务器状态失败！');
                  logger.error(err);
                }else{
                  logger.info('更新服务器状态成功！');
                }
              })
            });
          break;
        case 'saveSetting':
          var serverIp = req.body.serverIp;
          var serverPort = req.body.serverPort;
          var productName = req.body.productName;
          var serverUuid = req.body.serverUuid;
          var productId = req.body.productId;
          if(!serverIp || !serverPort ||!productName ||!serverUuid ||!productId ){
            res.json({success:false,code:'Lack of incoming parameters'})
          }else{
            GLOBAL.g_app_status.serverIp = serverIp;
            GLOBAL.g_app_status.serverPort = serverPort;
            GLOBAL.g_app_status.productName = productName;
            GLOBAL.g_app_status.serverUuid = serverUuid;
            GLOBAL.g_app_status.productId = productId;
            res.json({success:true,code:0})
          }
          break;
        case 'getPassword':
          var userPwd = req.query.userPwd;
          var userName = req.query.userName;
          if(!userPwd || !userName){
            res.json({success:false,code:'缺少传入参数！'})
          }else{
            if(userName == account && userPwd == password){
              res.json({success:true,code:'登录成功！'})
            }else{
              res.json({success:false,code:'用户密码错误！'})
            }
          }
          break;
        case 'changePwd':
          var userPwd = req.query.userPwd;
          var userName = req.query.userName;
          var userOldPwd = req.query.userOldPwd;
          if(!userPwd || !userName || !userOldPwd){
            res.json({success:false,code:'缺少传入参数！'})
          }else if(userOldPwd != password){
            res.json({success:false,code:'旧密码错误！'})
          }else{
            password = userPwd;
            res.json({success:true,code:'修改密码成功！'})
          }
          break;
        default:
          res.json({success:false,code:'not valid action'})
      }
    }catch(err){
      logger.error(err);
      res.json({success:false,code:e})
    }
  }
}

exports.format = function (time, format) {
  var a = new Date(time);
  a.setTime(a.getTime() - 28800000);
  var t = new Date(a.getTime());
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear()) + '-';
        break;
      case 'MM':
        return tf(t.getMonth() + 1) + '-';
        break;
      case 'mm':
        return tf(t.getMinutes()) + ':';
        break;
      case 'dd':
        return tf(t.getDate()) + ' ';
        break;
      case 'HH':
        return tf(t.getHours()) + ':';
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}