/**
 * Created by Rainc on 2015/4/25.
 */

var api = require('./api.js');
var mysql1 = require('./basedao.js');
var config = require('../etc/config.json');
var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

authNum15_serial_num = 1;   //15分钟流水号
authNum24_serial_num = 1;   //24小时流水号


exports.insertIntoTable = function(){
  //初始化时,等待10秒后，插入一笔数据
  setTimeout(function(){
    insert_15();
    insert_24();
  },10000)

  //每隔15分钟，判断tbl_pmd_authnum_15数据量是否大于设置值，大于或等于的话重头开始进行更新，否则插入一笔数据
  setInterval(function(){
    insert_15();
  },15 * 60 * 1000)

  //每隔24小时，判断tbl_pmd_authnum_24数据量是否大于设置值，大于或等于的话重头开始进行更新，否则插入一笔数据
  setInterval(function(){
    insert_24();
  },24 * 60 * 60 * 1000)
}

function insert_15(){
  var data =  GLOBAL.g_app_status;
  //检测该服务器数据是否大于设置值
  var sql = "select count(*) as total from tbl_pmd_authnum_15 where sys_uuid=" + data.serverUuid;
  //console.log(sql);
  mysql1.find(sql,function(err,rows){
    if(err){
      logger.error("查询tbl_pmd_authnum_15出错！");
      logger.error(err);
    }else{
      if(rows[0].total < config.authNum_count_limit){
        sql = "insert into tbl_pmd_authnum_15 (rec_status,sys_uuid,serial_no,generate_time,avg_process_time,max_process_time,total_process," +
        "cur_pending_cnt,cur_deal_cnt,cur_deal_status1,cur_deal_status2,cur_deal_status3,cur_deal_status4) values(" +
        0 + "," + data.serverUuid + "," + authNum15_serial_num + ",'" + api.format(new Date(),'yyyyMMddHHmmss') + "'," +
        data.avgProcessTime + "," + data.maxProcessTime + "," + data.totalProcess + ",'" + data.curPendingCnt + "','" + data.curDealCnt +
        "','" + data.curDealStatus1 + "','" + data.curDealStatus2 + "','" + data.curDealStatus3 + "','" + data.curDealStatus4 + "')";
        //console.log(sql);
        mysql1.process(sql);
        authNum15_serial_num ++;
      }else{
        if(authNum15_serial_num - 1 == config.authNum_count_limit){
          authNum15_serial_num = 1;
        }
        sql = "update tbl_pmd_authnum_15 set generate_time='" + api.format(new Date(),'yyyyMMddHHmmss') + "',avg_process_time=" +
        data.avgProcessTime + ",max_process_time=" + data.maxProcessTime + ",total_process=" + data.totalProcess + ",cur_pending_cnt='" +
        data.curPendingCnt + "',cur_deal_cnt='" + data.curDealCnt + "',cur_deal_status1='" + data.curDealStatus1 + "',cur_deal_status2='" +
        data.curDealStatus2 + "',cur_deal_status3='" + data.curDealStatus3 + "',cur_deal_status4='" + data.curDealStatus4 +
        "' where sys_uuid=" + data.serverUuid  + " and serial_no=" + authNum15_serial_num;
        //console.log(sql);
        mysql1.process(sql);
        authNum15_serial_num ++;
      }
    }
  })
}

function insert_24(){
  var data =  GLOBAL.g_app_status;
  //检测该服务器数据是否大于设置值
  var sql = "select count(*) as total from tbl_pmd_authnum_24 where sys_uuid=" + data.serverUuid;
  //console.log(sql);
  mysql1.find(sql,function(err,rows){
    if(err){
      logger.error("查询tbl_pmd_authnum_24出错！");
      logger.error(err);
    }else{
      if(rows[0].total < config.authNum_count_limit){
        sql = "insert into tbl_pmd_authnum_24 (rec_status,sys_uuid,serial_no,generate_time,avg_process_time,max_process_time,total_process," +
        "cur_pending_cnt,cur_deal_cnt,cur_deal_status1,cur_deal_status2,cur_deal_status3,cur_deal_status4) values(" +
        0 + "," + data.serverUuid + "," + authNum24_serial_num + ",'" + api.format(new Date(),'yyyyMMddHHmmss') + "'," +
        data.avgProcessTime + "," + data.maxProcessTime + "," + data.totalProcess + ",'" + data.curPendingCnt + "','" + data.curDealCnt +
        "','" + data.curDealStatus1 + "','" + data.curDealStatus2 + "','" + data.curDealStatus3 + "','" + data.curDealStatus4 + "')";
        //console.log(sql);
        mysql1.process(sql);
        authNum24_serial_num ++;
      }else{
        if(authNum24_serial_num - 1 == config.authNum_count_limit){
          authNum24_serial_num = 1;
        }
        sql = "update tbl_pmd_authnum_24 set generate_time='" + api.format(new Date(),'yyyyMMddHHmmss') + "',avg_process_time=" +
        data.avgProcessTime + ",max_process_time=" + data.maxProcessTime + ",total_process=" + data.totalProcess + ",cur_pending_cnt='" +
        data.curPendingCnt + "',cur_deal_cnt='" + data.curDealCnt + "',cur_deal_status1='" + data.curDealStatus1 + "',cur_deal_status2='" +
        data.curDealStatus2 + "',cur_deal_status3='" + data.curDealStatus3 + "',cur_deal_status4='" + data.curDealStatus4 +
        "' where sys_uuid=" + data.serverUuid  + " and serial_no=" + authNum24_serial_num;
        //console.log(sql);
        mysql1.process(sql);
        authNum24_serial_num ++;
      }
    }
  })
}

