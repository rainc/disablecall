#禁呼禁透服务

###状态写入到内存
###状态内容包括"产品信息"、"服务配置"、"系统信息"、"服务状态"
例子：
{
    avgProcessTime : '5 ms',
    maxProcessTime : '25 ms',
    totalProcess : 118,
    curPendingCnt : 0,
    startTime: '2015-03-16 01:07:06',
    productName: 'DCSERVER',
    serverIp: '114.114.114.114',
    serverPort: 4942,
    serverUuid: 1000,
    productId: 123,
    timestamp: '2015-03-16 01:09:01'
}

####产品信息里包含的信息
产品ID－－productId
产品名称－－productName
服务ID－－serverUuid

####系统信息里包含的信息
系统ip地址 - - serverIp
系统监听端口 - - serverPort

####服务状态
服务启动时间 - - startTime
处理总数 －－ totalProcess
当前待处理数量－－taskWaiting
最大处理时间 －－maxProcessTime
平均处理时间 －－avgProcessTime

#其他
当前时间戳 - - timestamp