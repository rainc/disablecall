/**
 * Created by Rainc on 14-2-27.
 */
var dgram = require('dgram');
var fs = require('fs');
var querystring = require('querystring');
var mysql1 = require('./controllers/basedao.js');
//var disableCallConfig = require('./etc/dcConfig.json');
var config = require('./etc/config.json');
var package = require('./package.json');
var radius = require('radius');
var async = require('async');

var log4js = require('log4js');
log4js.configure({
  appenders: [
    { type: 'console' }, //控制台输出
    {
      type: 'dateFile', //日志文件输出
      filename: config.appLogFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'DISABLECALL'
    }
  ],
  replaceConsole: true
});
var logger = log4js.getLogger('DISABLECALL');
logger.setLevel('INFO');

recordType = 3;  //录音机制类型（1为主叫模式，2为被叫模式，3为随机模式）
GLOBAL.g_app_status = {};     //状态信息
var tasks = 0;        //总处理数
var taskWaiting = 0;          //待处理数
var taskdealed = 0;          //已处理数
var dealStatus1 = 0;          //禁呼数
var dealStatus2 = 0;          //禁透数
var dealStatus3 = 0;          //禁呼禁透数
var dealStatus4 = 0;          //录音数
var dealTimes = 0;   //总处理时间
var maxDealTime = 0;  //最大处理时间
var startTime = 0;   //启动时间
var deviceNum = {};      //对接设备数
//var object = {};    //等待队列
var cacheObj = {};  //数据缓存队列
domain = {};    //域对应uuid与name缓存
var prevent = []; //防攻击缓存

var udp4 = dgram.createSocket('udp4');
exports.init = function(cb) {
  var isBindError = true;
  var obj = {
    product_id : config.product_id,
    server_id : config.server_id,
    status_path : config.status_path
  }
  udp4.bind(config.server_port, config.server_ip);
  udp4.on("error", function (err) {
    if(isBindError == true){
      cb(err,{});
    }
    console.log("udp server error:\n" + err.stack);
    udp4.close();
  });
  udp4.on('listening', function () {
    var address = udp4.address();
    loadCache('',function(err){
      if(err != null){
        logger.error(err);
      }
//      console.log(cacheObj);
    });
    loadDomain(function(err){
      if(err != null){
        logger.error(err);
      }
    })

//用于测试加入2百万条数据到缓存需要的时间，和2百万条查询一条使用的时间
//  for(var i = 0;i<2000000;i++){
//    var a = parseInt(Math.random() * 100000000000);
//    if(i ==0){
//      console.log(new Date().getTime())
//    }else if(i == 1999999){
//      cacheObj['1231244233'] =0 ;
//      console.log(new Date().getTime())
//      console.log(cacheObj['1231244233'])
//      console.log(new Date().getTime())
//    }
//    cacheObj[a] = 0;
//  }

//用于写入数据到本地文件
//  var fs = require('fs');
//  var array = [0,1,2];
//  var array1 = [1,2,3,4];
//  var i = 0;
//  var j = 0;
//  var txt_whole=''
//  var test = setInterval(function(){
//      var txt = '' + array[Math.round(Math.random()*(array.length-1))] + '\t' + parseInt(Math.random() * 100) + '\t' + parseInt(Math.random() * 100000000000) + '\t' + array1[Math.round(Math.random()*(array1.length-1))] + '\r\n';
//      txt_whole += txt;
//    i++;
//    if(i == 40000){
//      j++;
//      fs.appendFile('C:\\Users\\Rainc\\Desktop\\tbl_num.txt', txt_whole, function (err) {
//        if (err) throw err;
//        txt_whole = '';
//        i =0;
//            console.log('It\'s saved!'); //文件被保存
//      });
//      if(j == 6){
//        clearInterval(test);
//      }
//    }
//    },0)
    logger.info('App Server listening on ' + address.address + ":" + address.port);
    startTime = new Date().getTime();
    isBindError = false;
    cb(null,obj);
  });
};

udp4.on('message', function (message, remote) {
  //收集对接设备数据
  //var isInDeviceNum = false;
  //for(var i in deviceNum){
  //  if(deviceNum[i] == remote){
  //    isInDeviceNum = true;
  //    break;
  //  }
  //}
  //if(!isInDeviceNum){
  //  deviceNum.push(remote);
  //}

  var dealStartTime = process.hrtime();
  var retryNum = 1;

  deviceNum[remote.address] = remote.port;

  do_action(message,remote,dealStartTime,retryNum);
  //var myThread = function(message,remote,dealStartTime,retryNum){
  //  thread.end(do_action(message,remote,dealStartTime,retryNum))
  //};
  //var thread = tagg.create(myThread,function(err,res){
  //  if(err){
  //    console.log(err);
  //  }
  //  thread.destroy();//摧毁线程
  //});
});

//每5秒将当前状态信息存入内存中
setInterval(function(){
  //var plus = 0;
  //for(var i in dealTimes){
  //  plus += dealTimes[i]
  //}
  //var avarageDealTime = Math.round((plus/(dealTimes.length))*100)/100 || 0 ;
  var avarageDealTime = Math.round((dealTimes/taskdealed)*100)/100 || 0;
  GLOBAL.g_app_status = {
    avgProcessTime : avarageDealTime ,                //平均处理时长
    maxProcessTime : maxDealTime ,                    //最大处理时间
    totalProcess : tasks,                                    //总的处理数
    curPendingCnt : taskWaiting,                             //待处理数
    curDealCnt : taskdealed,                                 //已处理数
    curDealStatus1 : dealStatus1,                            //禁呼数
    curDealStatus2 : dealStatus2,                            //禁透数
    curDealStatus3 : dealStatus3,                            //禁呼禁透数
    curDealStatus4 : dealStatus4,                            //录音数

    startTime: format(startTime , 'yyyyMMddHHmmss'),
    productName: config.product_name,
    serverIp: config.server_ip,
    serverPort: config.server_port,
    serverUuid: config.server_id,
    productId: config.product_id,
    serverVersion: package.version,
    timestamp: format(new Date().getTime(),'yyyyMMddHHmmss')
  }
  //write into file
//  fs.writeFile(config.status_path, JSON.stringify(status),'utf8', function (err) {
//    if (err) throw err;
//  });
},5000)

function do_action(message,remote,dealStartTime,retryNum){
  tasks++;
  taskWaiting++;
//  console.log(message)
  action(message,remote,dealStartTime,function(err,result){
    //处理时间
    var dealTime = getHrtime(dealStartTime) / 1000000;
    //console.log('处理完成所用时间：'+dealTime );
    if(dealTime > maxDealTime){
      maxDealTime = dealTime;
    }
    dealTimes += parseInt(dealTime);

    if(err){
      logger.error(remote.address + '处理失败，正在重新处理！重试次数：' + retryNum);
      if(retryNum < 3){
        retryNum ++ ;
        do_action(message,remote,dealStartTime,retryNum);
      }else if(retryNum == 3){
        taskWaiting--;
        return;
      }
    }else if(result.status == 'success'){
//      console.log(remote.address + '处理成功！' + j++);
    }else if(result.status == 'wait'){
      console.log(remote.address + 'waiting...');
      var waitTime = setTimeout(function(){
        do_action(message,remote)
        clearTimeout(waitTime);
      },100)
    }
  })
}

function action(message,remote,dealStartTime,cb){
  var code, caller, called, undealCaller,undealCalled, name, packet, resStatus;
  //var isInObject = false;
  var secret = 'Number detection';
  var callerAction = null;
  var calledAction = null;
  packet = radius.decode_without_secret({packet: message,secret:'',no_secret:true});

  if (packet.code != 'Access-Request') {
    console.log('App Server unknown packet type: ', packet.code);
    return;
  }
  undealCaller = packet.attributes['Calling-Station-Id'];   //未经过号码处理的主叫
  undealCalled = packet.attributes['Called-Station-Id'];    //未经过号码处理的被叫
  caller = getPhoneNum(packet.attributes['Calling-Station-Id']);   //主叫
  called = getPhoneNum(packet.attributes['Called-Station-Id']);   //被叫
  name = packet.attributes['NAS-Identifier'];   //域名

  //logger.info('caller num: ' + undealCaller);
  //logger.info('called num: ' + undealCalled);

  //打印解析包所用时间
  //var packTime = process.hrtime();
  //console.log('解析包所用时间：' + getHrtime(dealStartTime)/1000000);

  //判断是否在防攻击列表中
//  console.log(prevent);
  var isInPrevent = false;
  for(var k in prevent){
    if(caller == prevent[k]['caller'] && called == prevent[k]['called'] && name == prevent[k]['name'] ){
      isInPrevent = true;
      if(prevent[k]['num'] >3){
        taskWaiting--;
        cb(null,{status:'fail',dealStartTime:dealStartTime})
        return;
      }else{
        prevent[k]['num'] ++;
      }
    }
  }
  if(isInPrevent == false){
    //加入防攻击缓存队列
    prevent.push({caller:caller,called:called,name:name,num:1});
  }
  //定时清除防攻击缓存队列
  var pt = setTimeout(function(){
//    for(var k in prevent){
//      if(prevent[k].num >3){
//        prevent.splice(k,1);
//      }
//    }
    prevent = [];
    clearTimeout(pt);
//    logger.info("防攻击缓存：" + JSON.stringify(prevent));
  },5000)

  //打印防攻击队列处理时间
  //var preventTime = process.hrtime();
  //console.log('防攻击队列所用时间：' + getHrtime(packTime)/1000000);

  //判断是否在等待队列
  //async.series([function(cb){
    //for(var i in object){
    //  if(i == caller || i == called){
    //    isInObject = true;
    //  }
    //}
  //  cb(null)
  //}],function(err){
    //if(isInObject == true){
    //  cb(null,{status:'wait',dealStartTime:dealStartTime});
    //}else{
      //查询缓存数组arr
      async.series([
        function(cb){
          //将处理中的号码放到等待队列
          //object[caller] = {};
          //object[called] = {};

          var domain_uuid = domain[name];
//          console.log(name,domain_uuid,caller,called);
          if(!domain_uuid){
            //出错，将号码移除等待队列
            //for(var i in object){
            //  if(i == caller){
            //    delete object[caller];
            //  }else if(i == called){
            //    delete  object[called];
            //  }
            //}
            cb("App Server can not find domain_uuid for name:" + name);
          }else{
            if(cacheObj[domain_uuid] != undefined){
              if(cacheObj[domain_uuid][caller] != undefined){
                callerAction = cacheObj[domain_uuid][caller];
              }
              if(cacheObj[domain_uuid][called] != undefined){
                calledAction = cacheObj[domain_uuid][called];
              }
            }
            if(callerAction == null){
              callerAction = 0;
            }
            if(calledAction == null){
              calledAction = 0;
            }
            cb(null);
          }
//          for(var i in cacheObj){
//            if(i == caller){
//              callerAction = cacheObj[i];
//            }else if(i == called){
//              calledAction = cacheObj[i];
//            }
//          }
        }
        //function(cb){
//        logger.info(callerAction,cacheObj);
          //若缓存里面没有改号码数据，则默认通过
          //if(callerAction == null){
          //  callerAction = 0;
          //}
          //if(calledAction == null){
          //  calledAction = 0;
          //}
          //cb(null);

//            if(callerAction == null || calledAction == null){
//            var sql = 'select num,action from tbl_num where num = ' + caller + ' or num = ' + called;
//            mysql1.find(sql, function (err, results) {
//              if (err) {
//                cb(err);
//              } else {
//                if (results == '') {
//                  cacheObj[caller] = 0;
//                  cacheObj[called] = 0;
//                  callerAction = 0;
//                  calledAction = 0;
//                  cb(null);
//                } else {
//                  if(results[1] != undefined){
//                    if(results[0].num == caller && results[1].num == called){
//                      cacheObj[caller] = results[0].action;
//                      cacheObj[called] = results[1].action;
//                      callerAction = results[0].action;
//                      calledAction = results[1].action;
//                    }else if(results[1].num == caller && results[0].num == called){
//                      cacheObj[caller] = results[1].action;
//                      cacheObj[called] = results[0].action;
//                      callerAction = results[1].action;
//                      calledAction = results[0].action;
//                    }
//                  }else{
//                    if(results[0].num == caller){
//                      cacheObj[caller] = results[0].action;
//                      cacheObj[called] = 0;
//                      callerAction = results[0].action;
//                      calledAction = 0;
//                    }else if(results[0].num == called){
//                      cacheObj[called] = results[0].action;
//                      cacheObj[caller] = 0;
//                      calledAction = results[0].action;
//                      callerAction = 0;
//                    }
//                  }
//                  cb(null);
//                }
//              }
//            })
//          }else{
//            cb(null)
//          }
//        }
      ],function(err){
        if (err) {
          logger.error('查询号码出错！' + JSON.stringify(err));
          cb(err);
        } else {
          if (callerAction == 0 && calledAction == 0) {
            code = 'Access-Accept';
            resStatus = 0;
          } else if((callerAction == 1 && calledAction==2) || (callerAction == 2 && calledAction==1) || callerAction == 3 || calledAction == 3){
            code = 'Access-Reject';
            resStatus = 3;          //禁呼禁透
            dealStatus3 ++;
          } else if (callerAction == 1 || calledAction == 1) {
            code = 'Access-Reject';
            resStatus = 1;          //禁呼
            dealStatus1 ++;
          } else if(callerAction == 2 || calledAction == 2){
            code = 'Access-Reject';
            resStatus = 2;         //禁透
            dealStatus2 ++;
          } else {
            //录音机制处理
            dealStatus4 ++;
            if(recordType == 1){
              if(callerAction == 4){
                code = 'Access-Accept';
                resStatus = 1;
              }else{
                code = 'Access-Accept';
                resStatus = 0;
              }
            }else if(recordType == 2){
              if(calledAction == 4){
                code = 'Access-Accept';
                resStatus = 1;
              }else{
                code = 'Access-Accept';
                resStatus = 0;
              }
            }else{
              if(callerAction == 4 || calledAction == 4){
                code = 'Access-Accept';
                resStatus = 1;
              }else{
                code = 'Access-Accept';
                resStatus = 0;
              }
            }
          }
          //当话单中的实际呼叫次数大于频率控制次数，进行频率控制
          if(frequency[domain[name]] && call_bill[domain[name]]){
            if(frequency[domain[name]]['1'] && call_bill[domain[name]]['1']){
              if(frequency[domain[name]]['1']['1'] && call_bill[domain[name]]['1']['1']){
                if(frequency[domain[name]]['1']['1'][undealCaller] && call_bill[domain[name]]['1']['1'][undealCaller]){
                  if(frequency[domain[name]]['1']['1'][undealCaller]['max_num'] <= call_bill[domain[name]]['1']['1'][undealCaller].length){
                    code = 'Access-Reject';
                    resStatus = 4;   //主叫超过频率限制
                  }
                }
                if(frequency[domain[name]]['1']['1']['domain'] && call_bill[domain[name]]['1']['1']['domain']){
                  if(frequency[domain[name]]['1']['1']['domain']['max_num'] <= call_bill[domain[name]]['1']['1']['domain'].length){
                    code = 'Access-Reject';
                    resStatus = 4;   //主叫超过频率限制
                  }
                }
              }
            }
            if(frequency[domain[name]]['2'] && call_bill[domain[name]]['2']){
              if(frequency[domain[name]]['2']['1'] && call_bill[domain[name]]['2']['1']){
                if(frequency[domain[name]]['2']['1'][undealCalled] && call_bill[domain[name]]['2']['1'][undealCalled]){
                  if(frequency[domain[name]]['2']['1'][undealCalled]['max_num'] <= call_bill[domain[name]]['2']['1'][undealCalled].length){
                    code = 'Access-Reject';
                    resStatus = 5;   //被叫超过频率限制
                  }
                }
                if(frequency[domain[name]]['2']['1']['domain'] && call_bill[domain[name]]['2']['1']['domain']){
                  if(frequency[domain[name]]['2']['1']['domain']['max_num'] <= call_bill[domain[name]]['2']['1']['domain'].length){
                    code = 'Access-Reject';
                    resStatus = 5;   //被叫超过频率限制
                  }
                }
              }
            }
          }

          var response = radius.encode_response({
            packet: packet,
            code: code,
            secret: secret,
            attributes: [
              ['ERRORCODE', resStatus]
            ]
          });

          //打印判断缓存时间
          //var cacheTime = process.hrtime();
          //console.log('处理缓存时间：' + getHrtime(preventTime)/1000000);

//          console.log('Sending ' + code + ' for ' + remote.address + ':' + remote.port);
//        console.log(cacheObj);
//        console.log(callerAction,calledAction);
          udp4.send(response, 0, response.length, remote.port, remote.address, function (err, bytes) {
            //打印发送完成所用时间
            //console.log('发送完成所用时间：' + getHrtime(cacheTime)/1000000);
            if (err) {
              console.log('Error sending response to ', remote);
              cb(err,{status:'fail',dealStartTime:dealStartTime});
            }else{
              //发送成功后，将该号码从等待队列删除
              //for(var i in object){
              //  if(i == caller){
              //    delete object[caller];
              //  }else if(i == called){
              //    delete  object[called];
              //  }
              //}
              cb(null,{status:'success',dealStartTime:dealStartTime});
            }
            taskWaiting--;
            taskdealed ++;
          });
        }
      })
    //}
  //})
}

function getHrtime(intime){
  var outime = process.hrtime(intime);
  var diff = outime[0] * 1e9 + outime[1]
  return diff;
}

function getPhoneNum(phoneNum) {
  var len = phoneNum.length;
  if (len < 10) {                  //特服号码
    if (len < 8) {
      return phoneNum.substr(-3, 3);
    } else {
      return phoneNum.substr(-5, 5);
    }
  } else if (len == 10) {          //固话
    return phoneNum.substr(-7, 7);
  } else {
    var reg = /^((0)((10|20|21|22|23|24|25|27|28|29|310|311|312|313|314|315|316|317|318|319|335|349|350|351|352|353|354|355|356|357|358|359|370|371|372|373|374|375|376|377|378|379|391|392|393|394|395|396|397|398|410|411|412|413|414|415|416|417|418|419|421|427|429|431|432|433|434|435|436|437|438|439|440|448|450|451|452|453|454|455|456|457|458|459|464|467|468|469|470|471|472|473|474|475|476|477|478|479|482|483|510|511|512|513|514|515|516|517|518|519|520|523|527|530|531|532|533|534|535|536|537|538|539|543|546|550|551|552|553|554|555|556|557|558|559|561|562|563|564|565|566|570|571|572|573|574|575|576|577|578|579|580|591|592|593|594|595|596|597|598|599|631|632|633|634|635|660|661|662|663|668|691|692|701|710|711|712|713|714|715|716|717|718|719|722|724|727|728|730|731|732|733|734|735|736|737|738|739|743|744|745|746|750|751|752|753|754|755|756|757|758|759|760|762|763|765|766|768|769|770|771|772|773|774|775|776|777|778|779|790|791|792|793|794|795|796|797|798|799|812|813|816|817|818|825|826|827|830|831|832|833|834|835|836|837|838|839|851|852|853|854|855|856|857|858|859|870|871|872|873|874|875|876|877|878|879|881|883|886|887|888|890|898|899|910|911|912|913|914|915|916|917|919|930|931|932|933|934|935|936|937|938|939|941|943|951|952|953|954|970|971|972|973|974|975|976|977|978|979|993)\d{7,8}))$/;
    if (reg.test(phoneNum)) {             //固话
      if (len == 11) {
        var reg1 = /^((0)((10|20|21|22|23|24|25|27|28|29)\d{8}))$/
        if (reg1.test(phoneNum)) {
          return phoneNum.substr(-8, 8);
        } else {
          return phoneNum.substr(-7, 7);
        }
      } else {
        return phoneNum.substr(-8, 8);
      }
    } else {                //手机号码
      if (len == 11) {
        return phoneNum;
      } else {
        return phoneNum.substr(-11, 11);
      }
    }
  }
}

function loadDomain(cb){
  //将数据库表数据载入缓存
  var sql = "select uuid,name from tbl_domain";
//  console.log(new Date().getTime());
  mysql1.find(sql,function(err,data){
    if(err){
      logger.error("load cache false!");
      cb(err);
    }else{
      for(var i in data){
        domain[data[i].name] = data[i].uuid;
      }
      cb(null);
    }
  })
}

function loadCache(uuid,cb){
//console.log(uuid);
  //将数据库表数据载入缓存
  if(uuid == undefined){
    cb('cannot set uuid== undefined');
  }else if(uuid == ''){
    var sql = "select domain_uuid,num,action from tbl_num";
//    console.log(new Date().getTime());
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error("load cache false!");
        cb(err);
      }else{
//        console.log(data);
        if(data.length== 0){
          cacheObj = [];
        }else{
          //清空缓存
          cacheObj = [];
          for(var i=0;i<data.length;i++){
            //if(i == 0){
            //  console.log(new Date().getTime());
            //}else if(i == 700000){
            //  console.log(new Date().getTime());
            //}
            if(cacheObj[data[i].domain_uuid] == undefined){
              cacheObj[data[i].domain_uuid] = {};
            }
            data[i].num = getPhoneNum(data[i].num);
            cacheObj[data[i].domain_uuid][data[i].num] = data[i].action;
          }
        }
        cb(null);
      }
    })
  }else{
    var sql = "select domain_uuid,num,action from tbl_num where domain_uuid=" + uuid;
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error("update cache domain_uuid=" + domain_uuid + " false!");
        cb(err);
      }else{
//        console.log(data);
        //把域给清空
        cacheObj[data[0].domain_uuid] = {};
        for(var i in data){
          if(cacheObj[data[i].domain_uuid] == undefined){
            cacheObj[data[i].domain_uuid] = {};
          }
          data[i].num = getPhoneNum(data[i].num);
          cacheObj[data[i].domain_uuid][data[i].num] = data[i].action;
        }
        cb(null);
      }
    })
  }
}

var format = function (time, format) {
  var a = new Date(time);
  //a.setTime(a.getTime() - a.getTimezoneOffset() * 60 * 1000);
  var t = new Date(a.getTime());
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear()) + '-';
        break;
      case 'MM':
        return tf(t.getMonth() + 1) + '-';
        break;
      case 'mm':
        return tf(t.getMinutes()) + ':';
        break;
      case 'dd':
        return tf(t.getDate()) + ' ';
        break;
      case 'HH':
        return tf(t.getHours()) + ':';
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}

exports.loadCache = function(uuid,cb){
  loadCache(uuid,function(err){
    if(err != null){
      logger.error(err);
      cb(err);
    }else{
      cb(null);
    }
  });
}


