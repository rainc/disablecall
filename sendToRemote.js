/**
 * Created by Rainc on 15-1-10.
 */

var fs = require('fs');
var crypto = require('crypto');
var querystring = require('querystring');
var http = require('http')
var config = require('./etc/config.json');

var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

exports.dcSendTo = function(cb){
//  fs.open(status_path,'r',function(err){
//    if(err){
//      console.log(err);
//      cb(err);
//    }else{
//      fs.readFile(status_path, 'utf8', function (err, data) {
//        if (err) {
//          throw err;
//          cb(err);
//        }else if(data){
  var data = GLOBAL.g_app_status;
  var num = 0;
  for(var i in data){
    num++;
  }
  if(num != 0){
    try{
      //  send to remote
      var postData = {
        Action:'UpdateStatusInfo',
        Version: data.serverVersion,
        Timestamp: format(new Date().getTime(),'yyyyMMddHHmmss'),
        Expires: format(new Date().getTime() + 300000,'yyyyMMddHHmmss'),
        serverUuid: server_id,
        productId: product_id,
        Signature: '',
        extendInfo:JSON.stringify(data)
        //startTime: data['startTime'],
        //totalProcess: data['totalProcess'],
        //curPendingCnt: data['curPendingCnt'],
        //maxProcessTime: parseInt(data['maxProcessTime'].toString().substring(0,1)),
        //avgProcessTime: parseInt(data['avgProcessTime'].toString().substring(0,1))
      };
      md5Str = 'Action='+ postData.Action + '&ServerUuid=' + postData.serverUuid + '&Version=' + postData.Version + '&Timestamp=' + postData.Timestamp +
        '&Expires=' + postData.Expires;
      postData.Signature = crypto.createHash('md5').update(md5Str).digest('hex');

      var options = {
        hostname: config.cloudServerHost,
        port: config.cloudServerPort,
        path: config.cloudServerPath + querystring.stringify(postData),
        method: 'GET'
      };
      var req = http.request(options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
        });
      });
      req.on('error', function(e) {
        logger.error('problem with request: ' + e.message);
        cb(e);
      });
      req.end();
      cb(null);
    }catch(e){
      logger.error(e);
      cb(e);
    }
  }
//        }
//      });
//    }
//  })
}

var format = function(time,format){
  var a = new Date(time);
  a.setTime(a.getTime() - 28800000);
  var t = new Date(a.getTime());
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear()) ;
        break;
      case 'MM':
        return tf(t.getMonth() + 1) ;
        break;
      case 'mm':
        return tf(t.getMinutes()) ;
        break;
      case 'dd':
        return tf(t.getDate()) ;
        break;
      case 'HH':
        return tf(t.getHours()) ;
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}