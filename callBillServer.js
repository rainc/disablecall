/**
 * Created by Rainc on 15-4-8.
 */
var dgram = require('dgram');
var config = require('./etc/config.json');
var radius = require('radius');
var mysql1 = require('./controllers/basedao.js');

var log4js = require('log4js');
log4js.configure({
  appenders: [
    { type: 'console' }, //控制台输出
    {
      type: 'dateFile', //日志文件输出
      filename: config.callBillLogFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'CALLBILL'
    }
  ],
  replaceConsole: true
});
var logger = log4js.getLogger('CALLBILL');
logger.setLevel('INFO');

/**
 * 扩展删除数组指定下标或指定对象
 */
Array.prototype.remove=function(obj){
  for(var i =0;i <this.length;i++){
    var temp = this[i];
    if(!isNaN(obj)){
      temp=i;
    }
    if(temp == obj){
      for(var j = i;j <this.length;j++){
        this[j]=this[j+1];
      }
      this.length = this.length-1;
    }
  }
}

frequency = {};               //号码频率控制表缓存
call_bill = {};      //频率控制的号码话单缓存
//timers = {};                  //定时器列表
callInfo = [];               //话单信息缓存

var udp4 = dgram.createSocket('udp4');
exports.init = function(cb) {
  udp4.bind(config.callBill_port, config.server_ip);

  udp4.on("error", function (err) {
    console.log("udp server error:\n" + err.stack);
    udp4.close();
    cb(err);
  });

  udp4.on('listening', function () {
    var address = udp4.address();
    logger.info('Call Bill Server listening on ' + address.address + ":" + address.port);

    loadFrequency('',0,function(err){
      if(err != null){
        logger.error(err);
      }
//      console.log(frequency,call_bill,timers)
    })

    //定时批量存储话单信息入库
    setInterval(function(){
      //构建sql语句
      var len = callInfo.length;
      var sql = "insert into tbl_call_bill (domain_uuid,caller,called,disconnect_cause,setup_time,alert_time,connect_time,disconnect_time,session_time,report_time,recv_time) values";
      if(len != 0){
        if(len == 1){
          sql += "(" + callInfo[0].domainUuid + ',' + callInfo[0].caller + "," + callInfo[0].called + "," + callInfo[0].disconnectCause + ",'" + callInfo[0].setupTime + "','" +
          callInfo[0].alertTime + "','" + callInfo[0].connectTime + "','" + callInfo[0].disconnectTime + "'," + callInfo[0].sessionTime + ",'" + callInfo[0].disconnectTime + "','" +
          callInfo[0].recvTime + "')";
        }else{
          for(var j = 0;j<len -1;j++){
            sql += "(" + callInfo[j].domainUuid + ',' + callInfo[j].caller + "," + callInfo[j].called + "," + callInfo[j].disconnectCause + ",'" + callInfo[j].setupTime + "','" +
            callInfo[j].alertTime + "','" + callInfo[j].connectTime + "','" + callInfo[j].disconnectTime + "'," + callInfo[j].sessionTime + ",'" + callInfo[j].disconnectTime + "','" +
            callInfo[j].recvTime + "'),";
          }
          sql += "(" + callInfo[len-1].domainUuid + ',' + callInfo[len-1].caller + "," + callInfo[len-1].called + "," + callInfo[len-1].disconnectCause + ",'" + callInfo[len-1].setupTime + "','" +
          callInfo[len-1].alertTime + "','" + callInfo[len-1].connectTime + "','" + callInfo[len-1].disconnectTime + "'," + callInfo[len-1].sessionTime + ",'" + callInfo[len-1].disconnectTime + "','" +
          callInfo[len-1].recvTime + "')";
        }
      }else{
        return;
      }

      //更新缓存call_bill
      for(var k = 0;k<callInfo.length;k++){
        if(frequency[callInfo[k].domainUuid]){
          if(frequency[callInfo[k].domainUuid]['1']){
            if(frequency[callInfo[k].domainUuid]['1']['1']) {
              //当频率控制列表中有这个号码时，在话单列表进行计数
              if (frequency[callInfo[k].domainUuid]['1']['1'][callInfo[k].caller]) {
                if (!call_bill[callInfo[k].domainUuid]) {
                  call_bill[callInfo[k].domainUuid] = {};
                }
                if (!call_bill[callInfo[k].domainUuid]['1']) {
                  call_bill[callInfo[k].domainUuid]['1'] = {};
                }
                if(!call_bill[callInfo[k].domainUuid]['1']['1']){
                  call_bill[callInfo[k].domainUuid]['1']['1'] = {};
                }
                if (!call_bill[callInfo[k].domainUuid]['1']['1'][callInfo[k].caller]) {
                  call_bill[callInfo[k].domainUuid]['1']['1'][callInfo[k].caller] = [];
                  call_bill[callInfo[k].domainUuid]['1']['1'][callInfo[k].caller].push({msgTime:new Date().getTime()})
                } else {
                  call_bill[callInfo[k].domainUuid]['1']['1'][callInfo[k].caller].push({msgTime:new Date().getTime()})
                }
              }else{
                if(frequency[callInfo[k].domainUuid]['1']['1']['domain']){
                  if(!call_bill[callInfo[k].domainUuid]){
                    call_bill[callInfo[k].domainUuid] = {};
                  }
                  if(!call_bill[callInfo[k].domainUuid]['1']){
                    call_bill[callInfo[k].domainUuid]['1'] = {};
                  }
                  if(!call_bill[callInfo[k].domainUuid]['1']['1']){
                    call_bill[callInfo[k].domainUuid]['1']['1'] = {};
                  }
                  //判断这个号码不是白名单号码时，进行计数
                  if(frequency[callInfo[k].domainUuid]['1']['2']){
                    if(!frequency[callInfo[k].domainUuid]['1']['2'][callInfo[k].caller]){
                      if(!call_bill[callInfo[k].domainUuid]['1']['1']['domain']){
                        call_bill[callInfo[k].domainUuid]['1']['1']['domain'] = [];
                        call_bill[callInfo[k].domainUuid]['1']['1']['domain'].push({msgTime:new Date().getTime()})
                      }else {
                        call_bill[callInfo[k].domainUuid]['1']['1']['domain'].push({msgTime:new Date().getTime()})
                      }
                    }
                  }else{
                    if(!call_bill[callInfo[k].domainUuid]['1']['1']['domain']){
                      call_bill[callInfo[k].domainUuid]['1']['1']['domain'] = [];
                      call_bill[callInfo[k].domainUuid]['1']['1']['domain'].push({msgTime:new Date().getTime()})
                    }else {
                      call_bill[callInfo[k].domainUuid]['1']['1']['domain'].push({msgTime:new Date().getTime()})
                    }
                  }
                }
              }
            }
          }
          if(frequency[callInfo[k].domainUuid]['2']){
            if(frequency[callInfo[k].domainUuid]['2']['1']) {
              //当频率控制列表中有这个号码时，在话单列表进行计数
              if (frequency[callInfo[k].domainUuid]['2']['1'][callInfo[k].called]) {
                if (!call_bill[callInfo[k].domainUuid]) {
                  call_bill[callInfo[k].domainUuid] = {};
                }
                if (!call_bill[callInfo[k].domainUuid]['2']) {
                  call_bill[callInfo[k].domainUuid]['2'] = {};
                }
                if(!call_bill[callInfo[k].domainUuid]['2']['1']){
                  call_bill[callInfo[k].domainUuid]['2']['1'] = {};
                }
                if (!call_bill[callInfo[k].domainUuid]['2']['1'][callInfo[k].called]) {
                  call_bill[callInfo[k].domainUuid]['2']['1'][callInfo[k].called] = [];
                  call_bill[callInfo[k].domainUuid]['2']['1'][callInfo[k].called].push({msgTime:new Date().getTime()})
                } else {
                  call_bill[callInfo[k].domainUuid]['2']['1'][callInfo[k].called].push({msgTime:new Date().getTime()})
                }
              }else{
                if(frequency[callInfo[k].domainUuid]['2']['1']['domain']){
                  if(!call_bill[callInfo[k].domainUuid]){
                    call_bill[callInfo[k].domainUuid] = {};
                  }
                  if(!call_bill[callInfo[k].domainUuid]['2']){
                    call_bill[callInfo[k].domainUuid]['2'] = {};
                  }
                  if(!call_bill[callInfo[k].domainUuid]['2']['1']){
                    call_bill[callInfo[k].domainUuid]['2']['1'] = {};
                  }
                  //判断这个号码不是白名单号码时，进行计数
                  if(frequency[callInfo[k].domainUuid]['2']['2']){
                    if(!frequency[callInfo[k].domainUuid]['2']['2'][callInfo[k].called]){
                      if(!call_bill[callInfo[k].domainUuid]['2']['1']['domain']){
                        call_bill[callInfo[k].domainUuid]['2']['1']['domain'] = [];
                        call_bill[callInfo[k].domainUuid]['2']['1']['domain'].push({msgTime:new Date().getTime()})
                      }else {
                        call_bill[callInfo[k].domainUuid]['2']['1']['domain'].push({msgTime:new Date().getTime()})
                      }
                    }
                  }else{
                    if(!call_bill[callInfo[k].domainUuid]['2']['1']['domain']){
                      call_bill[callInfo[k].domainUuid]['2']['1']['domain'] = [];
                      call_bill[callInfo[k].domainUuid]['2']['1']['domain'].push({msgTime:new Date().getTime()})
                    }else {
                      call_bill[callInfo[k].domainUuid]['2']['1']['domain'].push({msgTime:new Date().getTime()})
                    }
                  }
                }
              }
            }
          }
        }
      }
      callInfo = [];   //清空话单详细

      //入库
      mysql1.Cb_process(sql,null,function(err){
        if(err){
          logger.error('话单存入数据库错误！');
          logger.error(err);
        }else{
          logger.info('话单存入数据库成功！');
        }
      })
    },config.callInfoTime * 1000)

    //同时开启，每分钟检查一次，call_bill中超过frequency控制的，进行清空
    setInterval(function(){
      var nowTime = new Date().getTime();

      for(var i in call_bill){
        //主叫
        if(call_bill[i]['1']){
          if(call_bill[i]['1']['1']){
            for(var j in call_bill[i]['1']['1']){
              //var curLen = 0;
              if(call_bill[i]['1']['1'][j]){
                for(var k=0;k<call_bill[i]['1']['1'][j].length;k++){
                  if(frequency[i]['1']['1']){
                    if(frequency[i]['1']['1'][j]){
                      if((nowTime - call_bill[i]['1']['1'][j][k]['msgTime']) >= (frequency[i]['1']['1'][j]['time_limit'] * 60000)){
                        //当号码单时间超过频率控制时间，清除该数组元素
                        call_bill[i]['1']['1'][j].remove(k)
                      }
                    }
                  }
                }
                //if(frequency[i]['1']['1'][j]['max_num'] < curLen){
                //  call_bill[i]['1']['1'][j] = [];
                //}
              }
            }
          }
        }
        //被叫
        if(call_bill[i]['2']){
          if(call_bill[i]['2']['1']){
            for(var j in call_bill[i]['2']['1']){
              //var curLen = 0;
              if(call_bill[i]['2']['1'][j]){
                for(var k =0;k<call_bill[i]['2']['1'][j].length;k++){
                  if(frequency[i]['2']['1']){
                    if(frequency[i]['2']['1'][j]){
                      if((nowTime - call_bill[i]['2']['1'][j][k]['msgTime']) >= (frequency[i]['2']['1'][j]['time_limit'] * 60000)){
                        //当号码单时间超过频率控制时间，清除该数组元素
                        call_bill[i]['2']['1'][j].remove(k)
                      }
                    }
                  }
                }
                //if(frequency[i]['1']['1'][j]['max_num'] < curLen){
                //  call_bill[i]['1']['1'][j] = [];
                //}
              }
            }
          }
        }
      }
    },60 * 1000)

    cb(null);
  })
}

udp4.on('message', function (message, remote) {
  do_action(message,remote);
});

function loadFrequency(uuids,domain_uuid,cb){
  if(uuids == undefined) {
    cb('cannot set uuids == undefined');
  }else {
    //}else if(!uuids){
    //将数据库tbl_frequency表数据载入缓存
    var sql = "select call_num,role,time_limit,max_num,domain_uuid,freq_switch from tbl_frequency";
    if (domain_uuid != 0) {
      sql += ' where domain_uuid=' + domain_uuid;
    }
    mysql1.find(sql, function (err, data) {
      if (err) {
        logger.error("load frequency false!");
        cb(err);
      } else {
        //domain_uuid为0时，加载表中所有的数据，不为0时，加载这个域下的所有数据
        if (domain_uuid == 0) {
          //清空缓存
          frequency = {};
          call_bill = {};
          //for(var key in timers){
          //  clearInterval(timers[key]);
          //  delete timers[key];
          //}
          //加载表中所有数据
          for (var i = 0; i < data.length; i++) {
            if (frequency[data[i].domain_uuid] == undefined) {
              frequency[data[i].domain_uuid] = {};
            }
            if (frequency[data[i].domain_uuid][data[i].role] == undefined) {
              frequency[data[i].domain_uuid][data[i].role] = {};
            }
            if (frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch] == undefined) {
              frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch] = {};
            }
            frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch][data[i].call_num] = {
              max_num: data[i].max_num,
              time_limit: data[i].time_limit
            };        //频率控制列表缓存
            //启动定时器，同时加入定时器列表方便管理
            //var timer = setInterval(function(){
            //  if(call_bill[data[i].domain_uuid]){
            //    if(call_bill[data[i].domain_uuid][data[i].role]){
            //      if(call_bill[data[i].domain_uuid][data[i].role][data[i].call_num]){
            //        call_bill[data[i].domain_uuid][data[i].role][data[i].call_num] = 0;
            //      }
            //    }
            //  }
            //},data[i].time_limit * 60 * 1000);
            //var timerName = data[i].domain_uuid + '-' + data[i].role + '-' + data[i].call_num;
            //timers[timerName] = timer;
          }
        } else {
          //清除该域下所有缓存
          frequency[domain_uuid] = {};
          call_bill[domain_uuid] = {};
          //for(var key in  timers){
          //  var name = key.split('-');
          //  if(name[0] == domain_uuid){
          //    clearInterval(timers[key]);
          //    delete timers[key];
          //  }
          //}
          //加载该域中所有数据
          for (var i = 0; i < data.length; i++) {
            //重启或开启定时器
            //var timer = setInterval(function(){
            //  if(call_bill[data[i].domain_uuid]){
            //    if(call_bill[data[i].domain_uuid][data[i].role]){
            //      if(call_bill[data[i].domain_uuid][data[i].role][data[i].call_num]){
            //        call_bill[data[i].domain_uuid][data[i].role][data[i].call_num] = 0;
            //      }
            //    }
            //  }
            //},data[i].time_limit * 60 * 1000)
            //var timerName = data[i].domain_uuid + '-' + data[i].role + '-' + data[i].call_num;
            //timers[timerName] = timer;
            //添加或更新置频率控制列表
            if (frequency[data[i].domain_uuid][data[i].role] == undefined) {
              frequency[data[i].domain_uuid][data[i].role] = {};
            }
            if (frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch] == undefined) {
              frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch] = {};
            }
            frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch][data[i].call_num] = {
              max_num: data[i].max_num,
              time_limit: data[i].time_limit
            };
          }
        }
        cb(null);
      }
    })
  }
  //}else{
  //  var sql = "select domain_uuid,call_num,role,time_limit,max_num,freq_switch from tbl_frequency where domain_uuid = " + domain_uuid +
  //    " and uuid in (" + uuids + ") ";
  //  mysql1.find(sql,function(err,data){
  //    if(err){
  //      logger.error("load domain frequency false!");
  //      cb(err);
  //    }else{
  //      for(var i = 0 ;i<data.length;i++){
  //        //var timerName = data[i].domain_uuid + '-' + data[i].role + '-' + data[i].call_num;
  //        //判断频率控制列表中是否有这个号码，若有则删除该定时器
  //        //if(frequency[data[i].domain_uuid]){
  //        //  if(frequency[data[i].domain_uuid][data[i].role]){
  //        //    if(frequency[data[i].domain_uuid][data[i].role][data[i].call_num]){
  //        //      clearInterval(timers[timerName]);
  //        //      delete timers[timerName];
  //        //    }
  //        //  }
  //        //}
  //        //重启或开启定时器
  //        //var timer = setInterval(function(){
  //        //  if(call_bill[data[i].domain_uuid]){
  //        //    if(call_bill[data[i].domain_uuid][data[i].role]){
  //        //      if(call_bill[data[i].domain_uuid][data[i].role][data[i].call_num]){
  //        //        call_bill[data[i].domain_uuid][data[i].role][data[i].call_num] = 0;
  //        //      }
  //        //    }
  //        //  }
  //        //},data[i].time_limit * 60 * 1000)
  //        //timers[timerName] = timer;
  //        //添加或更新置频率控制列表
  //        if(frequency[data[i].domain_uuid] == undefined){
  //          frequency[data[i].domain_uuid] = {};
  //        }
  //        if(frequency[data[i].domain_uuid][data[i].role] == undefined){
  //          frequency[data[i].domain_uuid][data[i].role] = {};
  //        }
  //        if(frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch] == undefined){
  //          frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch] = {};
  //        }
  //        frequency[data[i].domain_uuid][data[i].role][data[i].freq_switch][data[i].call_num] =  {max_num :data[i].max_num,time_limit:data[i].time_limit};
  //      }
  //      cb(null);
  //    }
  //  })

  //同时将数据库中rec_status中状态为2的数据删除
  var sql = 'delete from tbl_frequency where rec_status = 2';
  mysql1.Cb_process(sql,null,function(err){
    if(err){
      logger.error("delete tbl_frequency rec_status = 2 false!");
      logger.error(err)
    }else{
      logger.info("delete tbl_frequency rec_status = 2 success!");
    }
  })
}

function do_action(message,remote){
  var code,sessionId,domainUuid,caller,called,setupTime,alertTime,connectTime,sessionTime,disconnectTime,disconnectCause,recvTime;
  var packet = radius.decode_without_secret({packet: message,secret:'',no_secret:true});

  if (packet.code != 'Accounting-Request') {
    console.log('CallBill Server unknown packet type: ', packet.code);
    return;
  }

  sessionId = packet.attributes['Acct-Session-Id'];
  caller = packet.attributes['Calling-Station-Id'];
  called = packet.attributes['Called-Station-Id'];
  domainUuid = domain[packet.attributes['NAS-Identifier']]; //域id
  disconnectCause = packet.attributes['disconnect-cause'];
  sessionTime = packet.attributes['Acct-Session-Time'];
  setupTime = packet.attributes['setup-time'];
  alertTime = packet.attributes['alert-time'];
  connectTime =  packet.attributes['connect-time'];
  disconnectTime =  packet.attributes['disconnect-time'];
  recvTime = format(new Date().getTime() - new Date().getTimezoneOffset()*60*1000 ,'yyyyMMddHHmmss');
  if(setupTime != '0000-00-00 00:00:00'){
    setupTime = format(new Date(packet.attributes['setup-time']).getTime() - new Date().getTimezoneOffset()*60*1000,'yyyyMMddHHmmss');
  }
  if(alertTime != '0000-00-00 00:00:00'){
    alertTime = format(new Date(packet.attributes['alert-time']).getTime() - new Date().getTimezoneOffset()*60*1000,'yyyyMMddHHmmss');
  }
  if(connectTime != '0000-00-00 00:00:00'){
    connectTime = format(new Date(packet.attributes['connect-time']).getTime() - new Date().getTimezoneOffset()*60*1000,'yyyyMMddHHmmss');
  }
  if(disconnectTime != '0000-00-00 00:00:00'){
    disconnectTime = format(new Date(packet.attributes['disconnect-time']).getTime() - new Date().getTimezoneOffset()*60*1000,'yyyyMMddHHmmss');
  }

  if(domainUuid == undefined){
    logger.error("Call Bill can not find domain_uuid for name:" + packet.attributes['NAS-Identifier'])
    return;
  }

  callInfo.push({sessionId:sessionId,caller:caller,called:called,domainUuid:domainUuid,setupTime:setupTime,alertTime:alertTime,connectTime:connectTime,
    sessionTime:sessionTime,disconnectTime:disconnectTime,disconnectCause:disconnectCause,recvTime:recvTime})

  //回复确认信息
  var response = radius.encode_response({
    packet: packet,
    code: 'Accounting-Response',
    secret:'frequency and call bill'
  });
  udp4.send(response, 0, response.length, remote.port, remote.address, function (err, bytes) {
    if (err) {
      console.log('Error Accounting sending response to ', remote);
    }else{
      console.log('send ' +  remote.address)
    }
  });
}

var format = function (time, format) {
  var a = new Date(time);
  a.setTime(a.getTime() - 28800000);
  var t = new Date(a.getTime());
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear()) + '-';
        break;
      case 'MM':
        return tf(t.getMonth() + 1) + '-';
        break;
      case 'mm':
        return tf(t.getMinutes()) + ':';
        break;
      case 'dd':
        return tf(t.getDate()) + ' ';
        break;
      case 'HH':
        return tf(t.getHours()) + ':';
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}

exports.loadFrequency = function(uuids,domain_uuid,cb){
  loadFrequency(uuids,domain_uuid,function(err){
    if(err != null){
      logger.error(err);
      cb(err);
    }else{
      cb(null);
    }
  });
}


