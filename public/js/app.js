/**
 * Created by Rainc on 15-1-5.
 */

$(document).ready(function() {
  //默认语言
  window.lang = new Lang('en','en',true);
  lan = window.location.search.split('?')[1] || '';

  //at the beginning,hide the statusFrom
  $('#dcStatus').show();
  $('#dcServerInfo').hide();


  //设置内容自适应屏幕高度
  $("#content").height($(window).height() * 0.85);

  //kendo中文化
  kendo.culture("zh-CN");

  //定时刷新状态信息
  setInterval(function(){
    $.ajax({url:'/api?action=getStatus',success: function(data,status){
      if(data.success == true){
        $("#startTime")[0].value = data.code.startTime;
        $("#totalProcess")[0].value = data.code.totalProcess;
        $("#curPendingCnt")[0].value = data.code.curPendingCnt;
        $("#maxProcessTime")[0].value = data.code.maxProcessTime + ' ms';
        $("#avgProcessTime")[0].value = data.code.avgProcessTime + ' ms';
//        $("#productName")[0].value = data.code.productName;
        $("#serverVersion")[0].value = data.code.serverVersion;
        $("#serverIp")[0].value = data.code.serverIp;
        $("#serverPort")[0].value = data.code.serverPort;
        $("#serverUuid")[0].value = data.code.serverUuid;
//        $("#productId")[0].value = data.code.productId;
//        $("#server")[0].innerHTML = data.code.productName + " | " + data.code.productId;
        $("#timeStamp")[0].innerHTML = data.code.timestamp;

        if(lan == 'CN'){
          $("#panelbar").kendoPanelBar({
            expandMode: "single",
            select: onPanelBarSelect,
            dataSource:[
              {
                text: data.code.productName + " | " + data.code.productId,
                expanded: true,
                items: [
                  { text: "号码系统信息"},
                  { text: "号码状态信息"}
                ]
              }
            ]
          });
        }else{
          $("#panelbar").kendoPanelBar({
            expandMode: "single",
            select: onPanelBarSelect,
            dataSource:[
              {
                text: data.code.productName + " | " + data.code.productId,
                expanded: true,
                items: [
                  { text: "DcServerInfo"},
                  { text: "DcStatusInfo"}
                ]
              }
            ]
          });
        }
      }
    }})
  },5000)

  //if no login info ,redirect to loginEN.html
  if($.cookie('userName') == undefined || $.cookie('userName') == null){
    location.href="loginCN.html?CN";
  }

  $("#startTime").kendoAutoComplete({});
  $("#totalProcess").kendoAutoComplete({});
  $("#curPendingCnt").kendoAutoComplete({});
  $("#maxProcessTime").kendoAutoComplete({});
  $("#avgProcessTime").kendoAutoComplete({});
  $("#serverIp").kendoAutoComplete({});
  $("#serverPort").kendoAutoComplete({});
//  $("#productName").kendoAutoComplete({});
  $("#serverVersion").kendoAutoComplete({});
  $("#serverUuid").kendoAutoComplete({});
//  $("#productId").kendoAutoComplete({});
  $("#logout").kendoButton();
  $("#changePwd").kendoButton();
  $("#setting").kendoButton();
  $("#saveSetting").kendoButton();

//  $("#toolbar").kendoToolBar({
//    resizable: true,
//    height: '50px',
//    items: [
//      {
//        template: "<div id='logo' style='font-size: 2.5em;margin-left: 20px'>DINSTAR</div>",
//        overflow: "never"
//      },
//      {
//        template: "<input id='dropdown' style='width: 130px;'/>",
//        overflow: "never"
//      },
//      {
//        type: "button",
//        text: "Logout",
//        id: "logout",
//        overflow: "always"
//      }
//    ],
//    click: toolBarClick
//  });

  $("#panelbar").kendoPanelBar({
    expandMode: "single",
    select: onPanelBarSelect,
    dataSource:[
      {
        text: 'SERVER',
        expanded: true,
        items: [
          { text: "DcServerInfo"},
          { text: "DcStatusInfo"}
        ]
      }
    ]
  });
  $("#dropdown").kendoDropDownList({
    dataTextField: "text",
    dataValueField: "value",
    select:onDropDownSelect,
    dataSource: [{ text: "中文", value: "1" },{ text: "EN", value: "2" }],
    index: 0 // 当前默认选中项，索引从0开始。
  });
  $("#dropdown").data("kendoDropDownList").select(1);

//判断初始语言
  if( lan == 'CN'){
    window.lang.dynamic('cn', 'json/langpack/cn.json');
    window.lang.change('cn');
    $("#logout")[0].innerHTML = "登出";
    $("#changePwd")[0].innerHTML = "修改密码";
    $("#dropdown").kendoDropDownList({
      dataTextField: "text",
      dataValueField: "value",
      select:onDropDownSelect,
      dataSource: [{ text: "中文", value: "1" },{ text: "EN", value: "2" }],
      index: 0 // 当前默认选中项，索引从0开始。
    });
    setTimeout(function(){
      $("#dropdown").data("kendoDropDownList").select(0)
    },200)
    $("#panelbar").kendoPanelBar({
      expandMode: "single",
      select: onPanelBarSelect,
      dataSource:[
        {
          text: '服务器',
          expanded: true,
          items: [
            { text: "号码系统信息"},
            { text: "号码状态信息"}
          ]
        }
      ]
    });
  }
});

function onPanelBarSelect(e){
  console.log($(e.item).find("> .k-link").text());
  if($(e.item).find("> .k-link").text() == '号码状态信息'|| $(e.item).find("> .k-link").text() == 'DcStatusInfo' ){
    $('#dcServerInfo').hide();
    $('#dcStatus').show();
  }else if($(e.item).find("> .k-link").text() == '号码系统信息'|| $(e.item).find("> .k-link").text() == 'DcServerInfo'){
    $('#dcStatus').hide();
    $('#dcServerInfo').show();
  }
}

function onDropDownSelect(e){
  var dataItem = this.dataItem(e.item.index());
  if(dataItem.text == 'CN' || dataItem.text == '中文'){
//    kendo.culture("zh-CN");
//    window.lang.dynamic('cn', 'json/langpack/cn.json');
//    window.lang.change('cn');
//    $("#logout")[0].innerHTML = "登出";
//    $("#dropdown").kendoDropDownList({
//      dataTextField: "text",
//      dataValueField: "value",
//      select:onDropDownSelect,
//      dataSource: [{ text: "中文", value: "1" },{ text: "英文", value: "2" }],
//      index: 0 // 当前默认选中项，索引从0开始。
//    });
//    setTimeout(function(){
//      $("#dropdown").data("kendoDropDownList").select(0)
//    },200)
    location.href = "index.html?CN"
  }else{
//    kendo.culture("en-US");
//    window.lang.change('en');
//    $("#logout")[0].innerHTML = "Logout";
//    $("#dropdown").kendoDropDownList({
//      dataTextField: "text",
//      dataValueField: "value",
//      select:onDropDownSelect,
//      dataSource: [{ text: "CN", value: "1" },{ text: "EN", value: "2" }],
//      index: 0 // 当前默认选中项，索引从0开始。
//    });
//    setTimeout(function(){
//      $("#dropdown").data("kendoDropDownList").select(1)
//    },200)
    location.href = "index.html?EN"
  }
}

function logOutClick(){
    $.cookie('userName',null);
    location.href="loginCN.html?CN";
}

function changePwdClick(){
  var win = $("#win");
  var html = '';
  html += '<ul id="win_changePwd" class="fieldlist" style="margin-top:10px"><li>' +
  '<label for="win_user_name" style="width:30%" lang="en">USER NAME:</label> ' +
  '<input id="win_user_name" value="admin" style="background-color: lightgrey;margin-left:-8px" disabled /></li>' +
  '<li><label for="win_old_password" style="width:30%" lang="en">USER OLD PASSWORD:</label>' +
  '<input id="win_old_password" type="password" value=""/></li>' +
  '<li><label for="win_password" style="width:30%" lang="en">USER PASSWORD:</label>' +
  '<input id="win_password" type="password" value=""/></li>' +
  '<li><label for="win_password_confirm" style="width:30%" lang="en">USER PASSWORD CONFIRM:</label>' +
  '<input id="win_password_confirm" type="password" value=""/></li>' +
  '</ul><center><button id="chpwd_confirm" style="font-size: 1em;margin-top:10px" lang="en" onclick="chPwd()">Confirm</button></center>';
  win.html(html);
  win.kendoWindow({
    width: "450px",
    height: "320px",
    title: getValue(lan,'changePwd'),
    actions: [
      "Pin",
      "Minimize",
      "Maximize",
      "Close"
    ]
  });
  win.data("kendoWindow").center().open();
  $("#win_user_name").kendoMaskedTextBox({});
  $("#win_old_password").kendoMaskedTextBox({});
  $("#win_password").kendoMaskedTextBox({});
  $("#win_password_confirm").kendoMaskedTextBox({});
  $("#chpwd_confirm").kendoButton({});
}

function chPwd(){
  var userOldPwd = $.md5($('#win_old_password').val());
  var userPwd = $.md5($('#win_password').val());
  var userPwdConfirm = $.md5($('#win_password_confirm').val());
  var userName = $('#win_user_name').val();

  if(userPwd != userPwdConfirm){
    window.alert(getValue(lan,'The two passwords differ!'));
    return;
  }else{
    $.ajax({
      url:'/api?action=changePwd&userName=' + userName + '&userPwd=' + userPwd + "&userOldPwd=" + userOldPwd ,
      type:'get',
      complete: function(XHR, TS){
        if(XHR.status == 200){
          var res_text = JSON.parse(XHR.responseText);
          if(res_text.success == true){
            $.cookie('userName',userName);
            $.cookie('userPwd', userPwd);
            $('#win').data("kendoWindow").close();
            window.alert(res_text.code);
          }else{
            window.alert(res_text.code);
          }
        }else{
          window.alert('网络连接出错！');
        }
      }
    })
  }
}

function enableSetting(){
  document.getElementById('serverIp').disabled = "";
  document.getElementById('serverPort').disabled = "";
//  document.getElementById('productName').disabled = "";
  document.getElementById('serverUuid').disabled = "";
//  document.getElementById('productId').disabled = "";
  document.getElementById('serverIp').style.backgroundColor = "";
  document.getElementById('serverPort').style.backgroundColor = "";
//  document.getElementById('productName').style.backgroundColor = "";
  document.getElementById('serverUuid').style.backgroundColor = "";
//  document.getElementById('productId').style.backgroundColor = "";
  $("#saveSetting").show();
  $("#setting").hide();
}

function saveSetting(){
  var data = {
    serverIp: document.getElementById('serverIp').value,
    serverPort: document.getElementById('serverPort').value,
//    productName: document.getElementById('productName').value,
    serverUuid: document.getElementById('serverUuid').value,
//    productId: document.getElementById('productId').value
  };
  $.post('/api?action=saveSetting',data,function(result){
    console.log(result);
    if(result.success == true){
      $("#setting").show();
      $("#saveSetting").hide();

      document.getElementById('serverIp').disabled = "disabled";
      document.getElementById('serverPort').disabled = "disabled";
//      document.getElementById('productName').disabled = "disabled";
      document.getElementById('serverUuid').disabled = "disabled";
//      document.getElementById('productId').disabled = "disabled";
      document.getElementById('serverIp').style.backgroundColor = "lightgrey";
      document.getElementById('serverPort').style.backgroundColor = "lightgrey";
//      document.getElementById('productName').style.backgroundColor = "lightgrey";
      document.getElementById('serverUuid').style.backgroundColor = "lightgrey";
//      document.getElementById('productId').style.backgroundColor = "lightgrey";
      alert('设置成功！');
    }else{
      alert('设置失败，err:' + result.code );
    }
  });
}

