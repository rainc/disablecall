/**
 * Created by Rainc on 15-1-5.
 */

$(document).ready(function() {
  //默认语言
  window.lang = new Lang('en','en',true);
  lan = window.location.search.split('?')[1] || '';

  //at the beginning,hide the statusFrom
  $('#dcStatus').show();
  $('#dcServerInfo').hide();


  //设置内容自适应屏幕高度
  $("#content").height($(window).height() * 0.85);

  //kendo中文化
  kendo.culture("zh-CN");

  //定时刷新状态信息
  setInterval(function(){
    $.ajax({url:'/api?action=getStatus',success: function(data,status){
      if(data.success == true){
        $("#startTime")[0].value = data.code.startTime;
        $("#totalProcess")[0].value = data.code.totalProcess;
        $("#curPendingCnt")[0].value = data.code.curPendingCnt;
        $("#maxProcessTime")[0].value = data.code.maxProcessTime;
        $("#avgProcessTime")[0].value = data.code.avgProcessTime;
        $("#productName")[0].value = data.code.productName;
        $("#serverVersion")[0].value = data.code.serverVersion;
        $("#serverIp")[0].value = data.code.serverIp;
        $("#serverPort")[0].value = data.code.serverPort;
        $("#serverUuid")[0].value = data.code.serverUuid;
        $("#productId")[0].value = data.code.productId;
//        $("#server")[0].innerHTML = data.code.productName + " | " + data.code.productId;
        $("#timeStamp")[0].innerHTML = data.code.timestamp;

        if(lan == 'CN'){
          $("#panelbar").kendoPanelBar({
            expandMode: "single",
            select: onPanelBarSelect,
            dataSource:[
              {
                text: data.code.productName + " | " + data.code.productId,
                expanded: true,
                items: [
                  { text: "号码系统信息"},
                  { text: "号码状态信息"}
                ]
              }
            ]
          });
        }else{
          $("#panelbar").kendoPanelBar({
            expandMode: "single",
            select: onPanelBarSelect,
            dataSource:[
              {
                text: data.code.productName + " | " + data.code.productId,
                expanded: true,
                items: [
                  { text: "dcServerInfo"},
                  { text: "dcStatusInfo"}
                ]
              }
            ]
          });
        }
      }
    }})
  },5000)



  $("#startTime").kendoAutoComplete({});
  $("#totalProcess").kendoAutoComplete({});
  $("#curPendingCnt").kendoAutoComplete({});
  $("#maxProcessTime").kendoAutoComplete({});
  $("#avgProcessTime").kendoAutoComplete({});
  $("#serverIp").kendoAutoComplete({});
  $("#serverPort").kendoAutoComplete({});
  $("#productName").kendoAutoComplete({});
  $("#serverVersion").kendoAutoComplete({});
  $("#serverUuid").kendoAutoComplete({});
  $("#productId").kendoAutoComplete({});
  $("#logout").kendoButton();
  $("#setting").kendoButton();
  $("#saveSetting").kendoButton();

//  $("#toolbar").kendoToolBar({
//    resizable: true,
//    height: '50px',
//    items: [
//      {
//        template: "<div id='logo' style='font-size: 2.5em;margin-left: 20px'>DINSTAR</div>",
//        overflow: "never"
//      },
//      {
//        template: "<input id='dropdown' style='width: 130px;'/>",
//        overflow: "never"
//      },
//      {
//        type: "button",
//        text: "Logout",
//        id: "logout",
//        overflow: "always"
//      }
//    ],
//    click: toolBarClick
//  });

  $("#panelbar").kendoPanelBar({
    expandMode: "single",
    select: onPanelBarSelect,
    dataSource:[
      {
        text: 'SERVER',
        expanded: true,
        items: [
          { text: "dcServerInfo"},
          { text: "dcStatusInfo"}
        ]
      }
    ]
  });
  $("#dropdown").kendoDropDownList({
    dataTextField: "text",
    dataValueField: "value",
    select:onDropDownSelect,
    dataSource: [{ text: "中文", value: "1" },{ text: "EN", value: "2" }],
    index: 0 // 当前默认选中项，索引从0开始。
  });
  $("#dropdown").data("kendoDropDownList").select(1);

//判断初始语言
  if( lan == 'CN'){
    window.lang.dynamic('cn', 'json/langpack/cn.json');
    window.lang.change('cn');
    $("#logout")[0].innerHTML = "登出";
    $("#dropdown").kendoDropDownList({
      dataTextField: "text",
      dataValueField: "value",
      select:onDropDownSelect,
      dataSource: [{ text: "中文", value: "1" },{ text: "EN", value: "2" }],
      index: 0 // 当前默认选中项，索引从0开始。
    });
    setTimeout(function(){
      $("#dropdown").data("kendoDropDownList").select(0)
    },200)
    $("#panelbar").kendoPanelBar({
      expandMode: "single",
      select: onPanelBarSelect,
      dataSource:[
        {
          text: '服务器',
          expanded: true,
          items: [
            { text: "号码系统信息"},
            { text: "号码状态信息"}
          ]
        }
      ]
    });
  }
});

function onPanelBarSelect(e){
  console.log($(e.item).find("> .k-link").text());
  if($(e.item).find("> .k-link").text() == '号码状态信息'|| $(e.item).find("> .k-link").text() == 'dcStatusInfo' ){
    $('#dcServerInfo').hide();
    $('#dcStatus').show();
  }else if($(e.item).find("> .k-link").text() == '号码系统信息'|| $(e.item).find("> .k-link").text() == 'dcServerInfo'){
    $('#dcStatus').hide();
    $('#dcServerInfo').show();
  }
}

function onDropDownSelect(e){
  var dataItem = this.dataItem(e.item.index());
  if(dataItem.text == 'CN' || dataItem.text == '中文'){
//    kendo.culture("zh-CN");
//    window.lang.dynamic('cn', 'json/langpack/cn.json');
//    window.lang.change('cn');
//    $("#logout")[0].innerHTML = "登出";
//    $("#dropdown").kendoDropDownList({
//      dataTextField: "text",
//      dataValueField: "value",
//      select:onDropDownSelect,
//      dataSource: [{ text: "中文", value: "1" },{ text: "英文", value: "2" }],
//      index: 0 // 当前默认选中项，索引从0开始。
//    });
//    setTimeout(function(){
//      $("#dropdown").data("kendoDropDownList").select(0)
//    },200)
    location.href = "index.html?CN"
  }else{
//    kendo.culture("en-US");
//    window.lang.change('en');
//    $("#logout")[0].innerHTML = "Logout";
//    $("#dropdown").kendoDropDownList({
//      dataTextField: "text",
//      dataValueField: "value",
//      select:onDropDownSelect,
//      dataSource: [{ text: "CN", value: "1" },{ text: "EN", value: "2" }],
//      index: 0 // 当前默认选中项，索引从0开始。
//    });
//    setTimeout(function(){
//      $("#dropdown").data("kendoDropDownList").select(1)
//    },200)
    location.href = "index.html?EN"
  }
}

function logOutClick(){
  location.href="/login?lan=CN";
}

function enableSetting(){
  document.getElementById('serverIp').disabled = "";
  document.getElementById('serverPort').disabled = "";
  document.getElementById('productName').disabled = "";
  document.getElementById('serverUuid').disabled = "";
  document.getElementById('productId').disabled = "";
  document.getElementById('serverIp').style.backgroundColor = "";
  document.getElementById('serverPort').style.backgroundColor = "";
  document.getElementById('productName').style.backgroundColor = "";
  document.getElementById('serverUuid').style.backgroundColor = "";
  document.getElementById('productId').style.backgroundColor = "";
  $("#saveSetting").show();
  $("#setting").hide();
}

function saveSetting(){
  var data = {
    serverIp: document.getElementById('serverIp').value,
    serverPort: document.getElementById('serverPort').value,
    productName: document.getElementById('productName').value,
    serverUuid: document.getElementById('serverUuid').value,
    productId: document.getElementById('productId').value
  };
  $.post('/api?action=saveSetting',data,function(result){
    console.log(result);
    if(result.success == true){
      $("#setting").show();
      $("#saveSetting").hide();

      document.getElementById('serverIp').disabled = "disabled";
      document.getElementById('serverPort').disabled = "disabled";
      document.getElementById('productName').disabled = "disabled";
      document.getElementById('serverUuid').disabled = "disabled";
      document.getElementById('productId').disabled = "disabled";
      document.getElementById('serverIp').style.backgroundColor = "lightgrey";
      document.getElementById('serverPort').style.backgroundColor = "lightgrey";
      document.getElementById('productName').style.backgroundColor = "lightgrey";
      document.getElementById('serverUuid').style.backgroundColor = "lightgrey";
      document.getElementById('productId').style.backgroundColor = "lightgrey";
      alert('设置成功！');
    }else{
      alert('设置失败，err:' + result.code );
    }
  });
}

