/**
 * Created by Rainc on 15-1-6.
 */

var lan = window.location.search.split('?')[1];

$(document).ready(function() {

  $("#userName").kendoAutoComplete({
  });

  $("#password").kendoAutoComplete({
  });

  $("#login").kendoButton({
//    click: onLoginClick
  });

  $("#language").kendoMenu({
    select: onLanguageSelect
  });
});

function onLanguageSelect(e){
//  console.log($(e.item).children(".k-link").text());
  if($(e.item).children(".k-link").text() == 'English' || $(e.item).children(".k-link").text() == '英文'){
    location.href = "/login?lan=EN"
  }else if($(e.item).children(".k-link").text() == 'Chinese' || $(e.item).children(".k-link").text() == '中文'){
    location.href = "/login?lan=CN"
  }
}